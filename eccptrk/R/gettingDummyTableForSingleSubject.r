gettingDummyTableForSingleSubject=function(dat,listAtt=NULL,suj,endPoint=NULL, timeColumn="Time_Relative_sf",durationColumn="Duration_sf",subjectColumn="Subject",behaviorColumn="Behavior")
{

    dat1=dat[dat[,subjectColumn]==suj,]
    dat2=dat1
    if(is.null(endPoint)){endPoint=max(dat2[,timeColumn]+dat2[,durationColumn])+1}
    
    # phase selection
   # dat2[dat2[,"Behavior"]==phase,c("Event_Type",timeColumn)]
    dummyTable=matrix(0,length(listAtt),endPoint)
    rownames(dummyTable)=listAtt
    colnames(dummyTable)=1:dim(dummyTable)[2]

    for(i in 1:length(listAtt))
    { 
        datAtt=dat2[dat2[,behaviorColumn]==listAtt[i],]
        if(dim(datAtt)[1]>0)
        {
          
            nbOcc=dim(datAtt)[1]
            for(occ in 1:nbOcc)
            {
              
                firstCol=datAtt[occ,timeColumn]+1
                
                lastCol=min(dim(dummyTable)[2],datAtt[occ,timeColumn]+datAtt[occ,durationColumn]+1)
                
                if(!is.na(firstCol) & !is.na(lastCol))
                {
                    dummyTable[i,firstCol:lastCol]=1 
                }
               
            }
            
        }
    }
return(dummyTable)
}