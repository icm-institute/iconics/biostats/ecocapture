#' getting curves from an occurrence table
#' @param x object resulting from occurrenceTable (see \link{occurrenceTable})
#' @param listAtt vector of attributes to be selected in the behavior column
#' @param listColors vector of colors to be used in the plot
#' @param smoothing "none" or strong
#' @param main title of the plot
#' @param offset offset for the legend
#' @param ... Further plot parameters
#' @examples
#' data_test=data.frame(Time_Relative_sf=rep(1:10,3),Duration_sf=rep(1,30),
#' Subject=rep(c("a","b","c"),each=10),Behavior=sample(c("happy","sad","na"),30,
#' replace=TRUE))
#' occ=occurrenceTable(data_test)
#' plot(occ)
#' @export
#' @importFrom graphics axis layout legend lines par plot rect
#' @importFrom grDevices dev.off png rainbow
#' @importFrom stats aggregate smooth.spline
plot.occurrenceTable=function(x, listAtt=NULL,listColors=NULL,smoothing="none",main=NULL,offset=0,...)
{
    if(is.null(listAtt)){listAtt=rownames(x$occ)}
    if(is.null(listColors)){listColors=rainbow(length(listAtt));names(listColors)=listAtt}
    # nf <- layout(matrix(c(1,1,2,1,1,2), 2, 3, byrow = TRUE), respect = TRUE)
    #    def.par <- par(no.readonly = TRUE) #
        attToDisplay=c()
        durAtt=c()
        maxTime=ncol(x$occ) 
        par(las=1)
        par(mar=c(3,3,3,10))
        plot(NULL,xlim=c(0,maxTime),bty="n",ylim=c(0,max(x$occ,na.rm=TRUE)),ylab="Number of subjects",main=paste0(main," (",x$nsuj," subjects)"),...)
        axis(side = 1,col="grey",line=0)
        axis(side = 2,col="grey",line=0)
        grid(nx = NULL, ny = NULL, col = "light grey", lty = "dotted",  lwd = par("lwd"), equilogs = TRUE)
        
        for( att in listAtt)
        {
            if(sum(x$occ[att,])!=0)
            {
                attToDisplay=c(attToDisplay,att)
                durAtt=c(durAtt,sum(x$occ[att,]))
                if(smoothing=="none")
                {
                    lines(1:maxTime,x$occ[att,],col=listColors[att])
                }
                if(smoothing=="strong")
                {
                    resSmoothing=smooth.spline(1:maxTime,x$occ[att,],spar=0.65)
                    lines(resSmoothing$x,resSmoothing$y,col=listColors[att])
                }
                if(smoothing=="medium")
                {
                    resSmoothing=smooth.spline(1:maxTime,x$occ[att,],spar=0.5)
                    lines(resSmoothing$x,resSmoothing$y,col=listColors[att])
                }
                if(smoothing=="low")
                {
                    resSmoothing=smooth.spline(1:maxTime,x$occ[att,],spar=0.45)
                    lines(resSmoothing$x,resSmoothing$y,col=listColors[att])
                }
                
            }
            
        }
        
        df=cbind(att=attToDisplay,dur=durAtt)
        resag=aggregate(as.numeric(as.character(df[,"dur"])),by=list(df[,"att"]),FUN="sum")
        resag2=resag[order(resag[,"x"],decreasing=TRUE),]
        attToDisplay2=resag2[,1]
        legend(x=maxTime+offset,y=max(x$occ,na.rm=TRUE),xpd=1,fill=listColors[attToDisplay2],legend=names(listColors[attToDisplay2]),cex=1,border="white")
}