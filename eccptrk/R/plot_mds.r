#' plots mds map
#'@param method classification method
#'@param n_group number of groups to obtain
#'@param subjects names of subjects to plot
#'@param res_dist result of distanceBySubject
#'@param legend.pos position of the legend ("topleft","topright","bottomright","bottomleft")
#'@param main title of the plot
#'@export
#'@importFrom stats hclust as.dist cutree
#'@importFrom smacof mds
#'@export
#'@examples
#' data_test=data.frame(Time_Relative_sf=rep(1:10,3),
#' Duration_sf=rep(1,30),Subject=rep(c("a","b","c"),each=10),
#' Behavior=sample(c("happy","sad","na"),30,replace=TRUE))
#' res_dist=distanceBetweenSubjects(data_test)
#' plotMds(res_dist)

plotMds=function(res_dist,subjects=NULL,n_group=2,method="ward.D2",legend.pos="topright",main="MDS results")
{
    if(is.null(subjects)){subjects=rownames(res_dist)}
    if(sum(!subjects%in%rownames(res_dist))!=0){stop("subjects are not in the rownames of res_dist")}
    mdsmat=res_dist[subjects,subjects]
    reshclust=hclust(as.dist(mdsmat),method=method)
    vec_f=cutree(reshclust,k=n_group)
    col2= rainbow(n_group)[vec_f]
    resmds=mds(mdsmat)
    plot(resmds,col=col2,cex=0.8,pch=16,main=paste(main,"\nStress=",round(resmds$stress,digits=2)))
    legend(x=legend.pos, fill=rainbow(n_group),paste("Group",1:n_group))
    list_res=list(reshclust=reshclust,gp=vec_f,resmds=resmds)
    return(list_res)
}
