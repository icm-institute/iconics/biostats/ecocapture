#' estimates the distance between subjects from an ecocapture dataset
#' @param dataset ecocapture dataset
#' @param phases  vector containing the names of the phases to be selected in the distance calculation
#' @param listAtt vector containing the names of the attributes to be selected in the distance calculation
#' @param subjects vector containing the names of the subjects to be selected in the distance calculation
#' @param plot_opt "MDS" or "hclust". If "MDS" the mds biplot is plotted. If "hclust", the dendrogram is plotted
#' @param conv1 size of convolution window (can be a vector if several convolutions should be used)
#' @param method distance method. Can be "euclidean","manhattan","lv"
#' @param output "mat" or "suj"
#' @param subjectColumn Default to "Subject". Name of the column corresponding to the subject column
#' @param timeColumn  Default to "Time_Relative_sf". Name of the column corresponding to the subject column
#' @param durationColumn  Default to "Duration_sf". Name of the column corresponding to the subject column
#' @param behaviorColumn  Default to "Behavior". Name of the column corresponding to the subject column
#' @examples
#'  data_test=data.frame(Time_Relative_sf=rep(1:10,3),
#' Duration_sf=rep(1,30),Subject=rep(c("a","b","c"),each=10),
#' Behavior=sample(c("happy","sad","na"),30,replace=TRUE))
#' distanceBetweenSubjects(dataset=data_test)
#' @export
#' @importFrom stats convolve
#' @examples
#' data_test=data.frame(Time_Relative_sf=rep(1:10,3),
#' Duration_sf=rep(1,30),Subject=rep(c("a","b","c"),each=10),
#' Behavior=sample(c("happy","sad","na"),30,replace=TRUE))
#' distanceBetweenSubjects(data_test)

distanceBetweenSubjects=function(dataset,phases=NULL,listAtt=NULL,subjects=NULL,plot_opt="MDS",conv1=NULL,method="euclidean",output="mat",subjectColumn="Subject",behaviorColumn="Behavior",timeColumn="Time_Relative_sf",durationColumn="Duration_sf")
{
  # triangle=function(n)
  # {
  #   vec=rep(NA,n)
  #   if(n%%2==0)
  #   {
  #     if(k<((n+1)/2))
  #     {
  #       vec[k]=
  #     }
  #     if(k>((n+1)/2))
  #     {
  #       vec[k]=
  #     }
  #   }
  # }
  if(is.null(subjects)){subjects=unique(dataset[,subjectColumn])}
  if(is.null(listAtt)){listAtt=unique(dataset[,behaviorColumn])}
  # Obtaining the list of dummy tables
  #-----------------------------------
  if(is.null(phases)) #If all the phases are selected
  {
      res=lapply(subjects,function(x){     return(gettingDummyTableForSingleSubject(dat=dataset,listAtt=listAtt,suj=x,subjectColumn=subjectColumn,behaviorColumn=behaviorColumn,timeColumn=timeColumn,durationColumn=durationColumn) )})
      names(res)=subjects
      minim=min(sapply(res,function(x){return(dim(x)[2])}))
      res2=lapply(res,function(x){x[,1:minim]})
      res3=res2
  }
  else
  {
      for(k in 1:length(phases))#If only some phases are selected (subsetByPeriod is used)
      {
          datasetphase=subsetByPeriod(dataset,listSubject=NULL,phase=phases[k],standardization="left", binding=TRUE,phaseDuration="minimal",subjectColumn=subjectColumn,behaviorColumn=behaviorColumn,timeColumn=timeColumn,durationColumn=durationColumn)$dataset
       #   for(i in 1:length(subjects))
       #   {
       #      d= gettingDummyTableForSingleSubject(dat=datasetphase,listAtt=listAtt,suj=subjects[i])
       #   }

          res=lapply(subjects,function(x){     return(gettingDummyTableForSingleSubject(dat=datasetphase,listAtt=listAtt,suj=x,subjectColumn=subjectColumn,behaviorColumn=behaviorColumn,timeColumn=timeColumn,durationColumn=durationColumn) )})
          names(res)=subjects
          minim=min(sapply(res,function(x){return(ifelse(is.null(dim(x)),length(x),dim(x)[2]))}))
          res2=lapply(res,function(x){if(!is.null(dim(x))){return(x[,1:minim])}else{return(x[1:minim])}})
          if(k==1){res3=res2}
          # binding if several phases
          if(k>1){ res3=lapply(1:length(res2),function(i){
                if(is.null(dim(res3[[i]])))
                {
                    return(c(res3[[i]],res2[[i]]))
                }
                else
                {
                    return(cbind(res3[[i]],res2[[i]]))
                }
          });

       }


     }
  }

   #if a convolution is used
    if(!is.null(conv1))
    {
        mat_conv=matrix(0,length(listAtt),ifelse(!is.null(dim(res3[[1]])[2]),dim(res3[[1]])[2],length(res3[[1]])))
        rownames(mat_conv)=listAtt
        for(att_conv in listAtt)
        {
            # mat_conv[att_conv,]=rep(0,ifelse(!is.null(dim(res3[[1]])[2]),dim(res3[[1]])[2],length(res3[[1]])))
            if(is.null(conv1))
            {
                mat_conv[att_conv,1]=1 # no convolution: window size = 1
            }
            else
            {
              #if(convolutionType=="rectangular")
              #{
                if(length(conv1)==length(listAtt)) # If one convolution window size by attribute
                {
                  names(conv1)=listAtt
                  mat_conv[att_conv,1:min(conv1[att_conv],dim(mat_conv)[2],na.rm=T)]=1
                }
                if(length(conv1)==1) # If one convolution window size for all attribute
                {
                  mat_conv[att_conv,1:min(conv1,dim(mat_conv)[2])]=1
                }
              #}
              # if(convolutionType=="triangular")
              # {
              #   if(length(conv1)==length(listAtt)) # If one convolution window size by attribute
              #   {
              #     names(conv1)=listAtt
              #     windowSize=min(conv1[att_conv],dim(mat_conv)[2],na.rm=T)
              #     mat_conv[att_conv,1:windowSize]=triangle(windowSize)
              #   }
              #   if(length(conv1)==1) # If one convolution window size for all attribute
              #   {
              #     windowSize=min(conv1[att_conv],dim(mat_conv)[2],na.rm=T)
              #     mat_conv[att_conv,1:windowSize]=triangle(windowSize)
              #   }
              # }


            }
        }

        listSujC=lapply(res3,function(y)
        {
            if(!is.null(dim(y)))
            {
                result=t(sapply(1:nrow(y),function(attr){convolve(y[attr,],mat_conv[attr,],type="open")}))
                return(result)
            }
            if(is.null(dim(y)))
            {
                return(convolve(y,mat_conv,type="open"))
            }
        })

        mat=distanceFromList(listSujC,conv1 = mat_conv,method=method)
        rownames(mat)=subjects;colnames(mat)=subjects
    }
    else
    {
        mat=distanceFromList(res3,conv1 = NULL,method=method)
        rownames(mat)=subjects;colnames(mat)=subjects
    }



    # Getting the distances between subjects
 #  mat=matrix(NA,length(subjects),length(subjects));rownames(mat)=subjects;colnames(mat)=subjects
 #  for(i in 1:length(subjects))
 #  {
 #     for(j in 1:length(subjects))
 #     {
 #         mat[i,j] =matricialDistance(res3[[i]],res3[[j]])
 #     }
 # }

 if(plot_opt=="MDS")
 {
     plot(mds(mat))
 }
 if(plot_opt=="hclust")
 {
     reshclust=hclust(as.dist(mat))
     plot(reshclust)
 }
 if(output=="mat")
 {
     return(mat)
 }
 if(output=="suj")
{
     names(listSujC)=subjects
 return(listSujC)
}

}

