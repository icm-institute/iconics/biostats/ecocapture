#' bandplot
#' @param dataset dataset with colnames as
#' @param main title of the plots
#' @param listAtt a vector containing the attributes to be present in the bandplot
#' @param listSubject a vector containing the subjects to be present in the bandplot
#' @param listColors a vector with the same size than listAtt to attribute a color to each attribute
#' @param tlim Vector containing. Default to c(0, max) Time point
#' @param displayLegend if FALSE, the legend is not displayed
#' @param displaySubject if FALSE, the subject names are not displayed
#' @param offset if legend is TRUE, horizontal offset for  the legend
#' @param onlyAttPresentInLegend displays only the attributes in legend
#' @param leftmargin Default 5. Left margin oof the bandplot when displaySubject=TRUE
#' @param rightmargin Default 5. Right margin of the bandplot when displayLegend=TRUE
#' @param subjectColumn Default to "Subject". Name of the column corresponding to the subject column
#' @param timeColumn  Default to "Time_Relative_sf". Name of the column corresponding to the subject column
#' @param durationColumn  Default to "Duration_sf". Name of the column corresponding to the subject column
#' @param behaviorColumn  Default to "Behavior". Name of the column corresponding to the subject column
#' @examples 
#' data_test=data.frame(Time_Relative_sf=rep(1:10,3),Duration_sf=rep(1,30),
#' Subject=rep(c("a","b","c"),each=10),Behavior=sample(c("happy","sad","na"),30,
#' replace=TRUE))
#' bandplot(data_test,main="Bandplot")
#' @export
#' @importFrom graphics grid
bandplot=function(dataset,listAtt=NULL,listSubject=NULL,listColors=NULL,tlim=NULL,main=NULL,displayLegend=TRUE,displaySubject=TRUE,onlyAttPresentInLegend=TRUE,offset=0,leftmargin=5,rightmargin=5, timeColumn="Time_Relative_sf",durationColumn="Duration_sf",subjectColumn="Subject",behaviorColumn="Behavior")
{
    res=dataset
    if(is.null(listSubject)){listSubject=levels(factor(dataset[,subjectColumn]))}
    if(is.null(listAtt)){listAtt=levels(factor(dataset[,behaviorColumn]))}
    if(is.null(listColors)){listColors= c(rainbow(8),rainbow(8,s=0.5),rainbow(max(c(1,length(listAtt)-16)),v=0.75))[1:length(listAtt)];names(listColors)=listAtt}
   
   # res=subsetByPeriod(dataset,listSubject=listSubject,phase=phase,normalisation=normalisation,binding=binding)
   # dataset=res$dataset    
    if(is.null(tlim))
    {
        maxTime=max(dataset[,timeColumn]+dataset[,durationColumn]+1)
        minTime=0
    }
    else{maxTime=tlim[2]+1;minTime=tlim[1]}
    
#    nf <- layout(matrix(c(1,1,2,1,1,2), 2, 3, byrow = TRUE), respect = TRUE)
#    def.par <- par(no.readonly = TRUE) #
    par(las=1)
    par(mar=c(3,ifelse(displaySubject,leftmargin,0),3,ifelse(displayLegend,rightmargin,0)))
    plot(NULL,xlim=c(minTime,maxTime),ylim=c(0,length(listSubject)+1),bty="n",yaxt="n",xlab="Time",ylab="",main=main)
    axis(side = 1,col="grey",line=0)
    if(displaySubject)
    {
        axis(side = 2,labels=listSubject,at=1:length(listSubject),cex.axis=0.8,col="grey",line=0)
    }
     grid(nx = NULL, ny = NULL, col = "light grey", lty = "dotted",  lwd = par("lwd"), equilogs = TRUE)
    
    attToDisplay=c()
    durAtt=c()
    for(subject in listSubject)
    {
        dataSubject=dataset[dataset[,subjectColumn]==subject,]
        ordSubject=1:length(listSubject)-0.5;names(ordSubject)=listSubject
        for(att in listAtt)
        {
            dataAtt=dataSubject[dataSubject[,behaviorColumn]==att &!is.na(        dataSubject[,behaviorColumn]),]
            # if(data_type=="raw")
            # {
            #     if(!is.null(dataAtt)&&dim(dataAtt)[1]>0)
            #     {
            #         nbOcc=dim(dataAtt)[1]/2
            #         for(occ in 1:nbOcc)
            #         {
            #             
            #             xleft=dataAtt[2*occ-1,"Time_Relative_sf"]+1
            #             xright=dataAtt[2*occ,"Time_Relative_sf"]+1
            #             ybottom=ordSubject[subject]
            #             ytop=ordSubject[subject]+1
            #             if(!is.na(xleft)&!is.na(xright))
            #             {
            #                 rect(xleft, ybottom, xright, ytop,col=listColors[att])
            #             }
            #             
            #         }
            #         
            #     }
            # }
            # if(data_type=="new")
            # {
               if(dim(dataAtt)[1]!=0)
               {    attToDisplay=c(attToDisplay,att)
                    durAtt=c(durAtt,sum(dataAtt[,durationColumn]))
                   for(i in 1:dim(dataAtt)[1]) 
                   {
                    
                       xleft=dataAtt[i,timeColumn]+1
                       xright=dataAtt[i,timeColumn]+1+dataAtt[i,durationColumn]
                       ybottom=ordSubject[subject]
                       ytop=ordSubject[subject]+0.9
                       if(!is.na(xleft)&!is.na(xright))
                       {
                           rect(xleft, ybottom, xright, ytop,col=listColors[att],border=NA)
                       }
                   }
               }
                         #}
        }
    }
    df=cbind(att=attToDisplay,dur=durAtt)
    resag=aggregate(as.numeric(as.character(df[,"dur"])),by=list(df[,"att"]),FUN="sum")
    resag2=resag[order(resag[,"x"],decreasing=TRUE),]
    attToDisplay2=resag2[,1]
    if(displayLegend)
    {
        if(onlyAttPresentInLegend)
        {
            legend(maxTime+1,length(listSubject),xpd=1,fill=listColors[attToDisplay2],legend=names(listColors[attToDisplay2]),cex=1,border="white")
        }
        else
        {
            legend(maxTime+offset,length(listSubject),xpd=1,fill=listColors,legend=names(listColors),cex=1,border="white")
            
        }
        }
  
}

